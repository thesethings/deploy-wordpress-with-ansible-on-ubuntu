# README #

### What is this repository for? ###

* Deploy WordPress with Apache, PHP, MySQL and wp-cli

### How to use ###

* Modify variables with your own given credentials, etc
    * roles/mysql/vars/main.yml
    * roles/wordpress/vars/main.yml
    * hosts
    
###### Start Deployment ######

<code>ansible-playbook playbook.yml -i hosts -u <target-host-user> -K</code>

##### Help #####
* If it's not working as expected, debug mode is automatically activated.
* Logfile is here: var/www/WPInstallDir/wp-content/debug.log
